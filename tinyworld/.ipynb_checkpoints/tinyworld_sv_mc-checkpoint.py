import numpy as np
from statistics import mean
from tinyworldenv import TinyWorld, LEFT, RIGHT, name_actions

if __name__ == "__main__":
    GAMMA = 0.9
    WORLD_SIZE = 4
    STARTING_STATE = 0
    NUM_TRIALS = 5000
    STEPS_PER_TRIAL = 100

    env = TinyWorld(WORLD_SIZE)

    cumulative_rewards = []
    for trial in range(NUM_TRIALS):
        env.reset(STARTING_STATE)
        actions = np.random.choice(2, STEPS_PER_TRIAL)
        rewards = []

        for action in actions:
            #print("Old state was {}".format(env.state))
            new_state, reward, _, _ = env.step(action)
            #print("After action {}, state is {} and reward is {}".
            #    format(name_actions[action], new_state, reward))
            rewards.append(reward)
        rewards_reversed = rewards[::-1]

        cumulative_reward = rewards_reversed[0]
        for reward in rewards_reversed[1:]:
            cumulative_reward = cumulative_reward*GAMMA + reward

        cumulative_rewards.append(cumulative_reward)

    #print(cumulative_rewards)
    print(mean(cumulative_rewards))


