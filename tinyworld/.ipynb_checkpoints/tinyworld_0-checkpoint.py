import numpy as np
from tinyworldenv import name_actions, TinyWorld

if __name__ == "__main__":
    GAMMA = 0.9
    env = TinyWorld()
    env.reset()

    actions = np.random.choice(2, 10)
    for action in actions:
        print("Old state was {}".format(env.state))
        new_state, reward, _, _ = env.step(action)
        print("After action {}, state is {} and reward is {}".
              format(name_actions[action], new_state, reward))




