LEFT = 0
RIGHT = 1
name_actions = {LEFT: 'left', RIGHT: 'right'}

class TinyWorld:
    def __init__(self, n = 2):
        assert n > 1
        self.state = 0
        self.n = n
        self.action_space = 2

    def reset(self, new_state=0):
        assert 0<= new_state < self.n
        self.state = new_state
        return self.state

    def step(self, action):
        reward = 0
        if action == LEFT:
            self.state -= 1
        elif action == RIGHT:
            self.state += 1
        else:
            raise Exception

        if self.state >= self.n:
            self.state = self.n - 1
            reward = -1

        if self.state < 0:
            self.state = 0
            reward = -1

        terminal = False
        info = None

        return self.state, reward, terminal, info


