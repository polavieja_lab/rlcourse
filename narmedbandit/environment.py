import numpy as np

class NArmedBandit:
    def __init__(self, means):
        assert len(means.shape) == 1
        self.num_arms = means.shape[0]
        self._means = means
        self._sigmas = np.ones(self.num_arms)

    def step(self, action):
        assert 0 <= action < self.num_arms
        return self._means[action] + self._sigmas[action]*np.random.randn()

