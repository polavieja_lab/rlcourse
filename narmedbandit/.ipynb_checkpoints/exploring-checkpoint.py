import numpy as np
import matplotlib.pyplot as plt
from environment import NArmedBandit

#np.random.seed(0)
#means = np.random.rand(10)
#print(means)
#env = NArmedBandit(means)


NUM_TRIALS = 1000
NUM_STEPS = 1000

def greedy_policy(Q):
    return np.argmax(Q)

def random_policy(Q):
    return np.random.choice(Q.shape[0])

def e_greedy_policy(Q, epsilon):
    if np.random.rand() < epsilon:
        return random_policy(Q)
    else:
        return greedy_policy(Q)

def play_environment(env, epsilon, num_steps=NUM_STEPS, optimistic_iv=0):

    num_times_visited = np.zeros(env.num_arms)
    #average_reward = np.zeros(env.num_arms)
    average_reward = np.ones(env.num_arms)*optimistic_iv

    rewards = np.zeros(num_steps)
    step = 0

    # Exploration and exploitation

    for _ in range(num_steps):
        action = e_greedy_policy(average_reward, epsilon)
        reward = env.step(action)
        num_times_visited[action] += 1
        average_reward[action] += (reward - average_reward[action])/num_times_visited[action]
        rewards[step] = reward
        step += 1

    return rewards

env = NArmedBandit(np.random.randn(10))
rewards_01 = play_environment(env, 0.1)
for trial in range(NUM_TRIALS - 1):
    env = NArmedBandit(np.random.randn(10))
    rewards_01 += play_environment(env, 0.1)

env = NArmedBandit(np.random.randn(10))
rewards_g = play_environment(env, 0.0)
for trial in range(NUM_TRIALS - 1):
    env = NArmedBandit(np.random.randn(10))
    rewards_g += play_environment(env, 0.0)


plt.plot(rewards_01/NUM_TRIALS, 'r')
plt.plot(rewards_g/NUM_TRIALS, 'k')

plt.show()

