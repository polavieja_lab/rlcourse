{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Q-Learning and Sarsa: The Cliff"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On this example we will use Sarsa and Q-learning to obtain the optimal policy in a slightly more difficult problem. In this problem, our environment is a small grid, and the agent needs to walk from the bottom left corner of the grid, to the bottom right, while avoiding falling into the cliff.\n",
    "\n",
    "\n",
    "![cliff.png](attachment:cliff.png)\n",
    "\n",
    "Each valid step yields a negative reward of -1, except when falling off the cliff, that yields a negative reward of -100 and ends the episode. Each invalid step (going through a wall) yields a negative reward of -1 and leaves the agent in the same place. It the goal is achieved, reward is 100 and the episode ends."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Import libraries and environment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we need to import the python libraries that will be used along the notebook.\n",
    "We will use Numpy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np # Numpy is the standard Python library to do operation with arrays."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "as well as our own CliffWalking enviroment. You are encouraged to look at the environment file. However, from the enviroment we will only use three functions:\n",
    "\n",
    "    1.reset: resets the enviroment's variables and returns the initial observation\n",
    "    \n",
    "    2.step: performs an action and returns the observation\n",
    "    \n",
    "    3.visualize: visualizes a game through matplotlib "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from CliffWalking import CliffWalking # CliffWalking is the enviroment we created for this example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. The Agent class"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will implemment an agent class, that will be able to play the game by generating episodes on training mode or testing mode."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2.1 Initialize the agent and enviroment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For that we need to set:\n",
    "\n",
    "Enviroment's variables: the size of the cliff, number of available actions and optimal score.\n",
    "\n",
    "Exploration's variables: epsilon marks the probability the agent performs a random action. As they gather more knowledge over the enviroment, this probability decreases to a minimum, as their prediction accuracy increases and becomes more reliable. \n",
    "\n",
    "Update's variables: alpha is the learning rate of the agent and gamma the dicount rate.\n",
    "\n",
    "Policy: Q is a dict that pairs states with the Q-values for each action on that state.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Agent(): \n",
    "    def __init__(self, size, lm='SARSA'):\n",
    "        \n",
    "        #enviroment \n",
    "        self.grid_size = size\n",
    "        self.env = CliffWalking(self.grid_size) #Initializing the enviroment\n",
    "        self.action_space = [0,1,2,3]\n",
    "        self.actions_num = len(self.action_space)\n",
    "        self.optimal_score = self.env.goal_reward - self.grid_size[1]\n",
    "        \n",
    "        #exploration\n",
    "        self.epsilon = 0.1\n",
    "        \n",
    "        #update \n",
    "        self.alpha = 0.05\n",
    "        self.gamma = 1.0\n",
    "        \n",
    "        #policy\n",
    "        self.Q_sa = np.zeros((size[0],size[1],self.actions_num))#defaultdict(lambda: np.zeros(self.actions_num))\n",
    "        \n",
    "        #modes\n",
    "        self.learning_method = lm \n",
    "        \n",
    "        \n",
    "    def greedy_policy(self, s):\n",
    "        \"\"\" For a given state, s, the greedy policy takes the action that \n",
    "        maximises Q(s, a) \n",
    "        \"\"\"\n",
    "        return np.argmax(self.Q_sa[s])\n",
    "\n",
    "    def random_policy(self):\n",
    "        \"\"\" The random policy takes an action at random. In TinyWorld, there\n",
    "        are always two possible actions (left, right) \"\"\"\n",
    "        return np.random.choice(4) #self.action_space)\n",
    "\n",
    "    def e_greedy_policy(self, s):\n",
    "        \"\"\" This policy takes a random action with probability epsilon,\n",
    "        and otherwise it follows the greedy policy \"\"\"\n",
    "        if np.random.rand() < self.epsilon:\n",
    "            return self.random_policy()\n",
    "        else:\n",
    "            return self.greedy_policy(s)\n",
    "    \n",
    "    def choose_action(self, s, test=False):\n",
    "        if not test:\n",
    "            return self.e_greedy_policy(s)\n",
    "        else:\n",
    "            return self.greedy_policy(s)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2.3 Update Policies"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After performing the chosen action and observing the results, they need to evaluate whether or not that action was beneficial. If it was, the update function is going to reinforce it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2.3a Update Q-Learning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The update function for Q-Learning is the following:\n",
    "![Screenshot%20from%202018-12-07%2015-45-33.png](attachment:Screenshot%20from%202018-12-07%2015-45-33.png)\n",
    "which is easily implemented as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def update_policy_QL(self, current_state, next_state, action, reward):\n",
    "    self.Q_sa[current_state][action] += self.alpha * (reward + self.gamma * np.max(self.Q_sa[next_state]) - self.Q_sa[current_state][action])\n",
    "\n",
    "Agent.update_policy_QL = update_policy_QL\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2.3b Update Sarsa"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The update function for Sarsa is quite similar with one major difference.\n",
    "\n",
    "Since it's an on-policy method, the agent uses the same policy to predict actions and evaluate them: \n",
    "\n",
    "![Screenshot%20from%202018-12-07%2015-45-33-2.png](attachment:Screenshot%20from%202018-12-07%2015-45-33-2.png)\n",
    "\n",
    "So instead of getting the best next_action for the next_state greedily from a second policy as on Q-Learning (through 'max'), the agent will use the same policy to predict the next_action, update and then return it so that it will be performed on the next timestep:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def update_policy_Sarsa(self, current_state, next_state, action, next_action, reward):\n",
    "    self.Q_sa[current_state][action] += self.alpha * (reward + self.gamma * self.Q_sa[next_state][next_action] - self.Q_sa[current_state][action])\n",
    "        \n",
    "Agent.update_policy_Sarsa = update_policy_Sarsa"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2.4 Printing the results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For simplicity, we define three functions for printing the name of method under which we train, the best and average score, as well as visualize a game:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def print_method(self):\n",
    "    print('Training using', self.learning_method, 'method:\\n')\n",
    "        \n",
    "def print_best(self, best_score):\n",
    "    print(\"Using\", self.learning_method, \"generated best score: \",best_score)\n",
    "    if best_score == agent.optimal_score:\n",
    "        print(\"which is optimal!\")\n",
    "\n",
    "def print_game(self, best_episode):\n",
    "    self.env.visualize(best_episode) #show history of best episode\n",
    "            \n",
    "Agent.print_method = print_method\n",
    "Agent.print_best = print_best\n",
    "Agent.print_game = print_game"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Training by Playing the Game"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are going to use the agent class, to generate an episode using the basic principals:\n",
    "\n",
    "A. for 100 timesteps repeat:\n",
    "    1. choose an action based on policy\n",
    "    2. perform said action and observe results\n",
    "    3. log the results for later use\n",
    "    4. update the policy based on observation\n",
    "    5. if the goal was reached: finish the for loop\n",
    "B. return game history\n",
    "\n",
    "which in Python, looks like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "def generate_episode(self, test=False): #Runs an episode and returns the history, total score and success\n",
    "    state = self.env.reset()\n",
    "    action = self.choose_action(state, test)\n",
    "    score = 0\n",
    "    episode = []\n",
    "\n",
    "    for t in range(100):\n",
    "        next_state, reward, done = self.env.step(action)\n",
    "        episode.append((state, action, reward))\n",
    "        score += reward\n",
    "\n",
    "        next_action = self.choose_action(next_state, test)\n",
    "            \n",
    "        if not test: #update policy based on new observation\n",
    "            if self.learning_method == \"QL\":\n",
    "                self.update_policy_QL(state, next_state, action, reward) # Note that we do not use next action\n",
    "            elif self.learning_method == \"SARSA\":\n",
    "                self.update_policy_Sarsa(state, next_state, action, next_action, reward)\n",
    "            else:\n",
    "                raise Exception\n",
    "            \n",
    "        if done:\n",
    "            break\n",
    "        \n",
    "        action = next_action\n",
    "        state = next_state\n",
    "        \n",
    "    return episode, score, done\n",
    "        \n",
    "Agent.generate_episode = generate_episode"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. Testing the Policy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are going to test the policy's acurracy, by generating episodes with the previous function, but with the explore and learn boolean flags switched off.\n",
    "\n",
    "That way:\n",
    "    1. With explore = False, the agent always chooses an action greedily.\n",
    "    2. With learn = False, the agent doesn't update the policy.\n",
    "    \n",
    "So we get:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def generate_test_episode(self):\n",
    "    episode, score, done = generate_episode(self, test=True)\n",
    "    return episode, score, done\n",
    "\n",
    "Agent.generate_test_episode = generate_test_episode\n",
    "\n",
    "def test(self, i): #Every eval_frequency episodes, test our policy, by disabling learning and exploration\n",
    "    if i%eval_frequency==0 and not i==0 or i == total_episodes-1:\n",
    "        test_score = 0\n",
    "        for _ in range(eval_episodes):\n",
    "            history, score, done = generate_test_episode(self)\n",
    "            test_score += score\n",
    "            \n",
    "        print(\"During testing, average score is \", test_score/eval_episodes)\n",
    "\n",
    "\n",
    "Agent.test = test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5. Putting things together"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We specify the number of episodes for the taining and testing trials, and the evaluation frequency:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "total_episodes = 2000 #number of training episodes\n",
    "eval_frequency = 100 #evaluate every 100 training episodes,\n",
    "eval_episodes = 1 #by running 1 episode on test mode"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Only thing left, is the main loop. We create the agent class in a for loop, once for every method and then alternate between training and testing, while printing the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Training using QL method:\n",
      "\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "During testing, average score is  -12.0\n",
      "Using QL generated best score:  -12.0\n",
      "which is optimal!\n",
      "Average of scores during training: -43.982\n",
      "Episode run using the greedy policy and the final action-value function\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAW4AAACSCAYAAABlhSBZAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAACLxJREFUeJzt3X+oXnUBx/HPp/tju9PEiS7tbuQCXS1jLS5iDSJUYla0/ihwkEgI+ydrhhDWP9F//RFSf0gwdDloKDIHjVipmCGGLa9Ta3Mzxyq9utpCzR+s3c0+/fE8wu3uud4zd85z7vfp/YLLfZ6zs/P9HO7dZ+f5Puc8x0kEACjH+9oOAAA4MxQ3ABSG4gaAwlDcAFAYihsACkNxA0BhKG4AKAzFDQCFobgBoDDDTWx06choxheNNbFpABhIL504rldPTrvKuo0U9/iiMe1Ys66JTQPAQPrKM7+rvC5TJQBQGIobAApDcQNAYShuACgMxQ0AhaG4AaAwFDcAFIbiBoDCUNwAUJhKxW17ve3nbB+yfVvToQAAc5u3uG0PSbpD0nWSVkvaaHt108EAAL1VOeK+UtKhJIeTTEu6V9KGZmMBAOZSpbjHJb044/lUd9n/sL3J9qTtyVdPTteVDwAwS5Xi7vUxgzltQbIlyUSSiaUjo2efDADQU5XinpK0Ysbz5ZJebiYOAGA+VYr7CUmX2V5pe1TS9ZJ2NRsLADCXeW+kkOSU7ZslPSBpSNLWJPsbTwYA6KnSHXCS7Ja0u+EsAIAKuHISAApDcQNAYShuACgMxQ0AhaG4AaAwFDcAFIbiBoDCUNwAUBiKGwAKQ3EDQGEobgAoDMUNAIWhuAGgMBQ3ABSG4gaAwlDcAFAYihsACkNxA0BhKG4AKMy8xW17q+2jtvf1IxAA4N1VOeK+W9L6hnMAACqat7iTPCrplT5kAQBUUNsct+1NtidtT756crquzQIAZqmtuJNsSTKRZGLpyGhdmwUAzMJZJQBQGIobAApT5XTAeyQ9LmmV7SnbNzUfCwAwl+H5VkiysR9BAADVMFUCAIWhuAGgMBQ3ABSG4gaAwlDcAFAYihsACkNxA0BhKG4AKMy8F+C8F4t9QqtGDzWx6Z6OL3ujb2NJ0vGLX+/reCeW/buv4+nisT6Pt6R/Y12wrH9jSdJ5q/o83uV9HW7Rko/1dbwlS9b0dbyxsY/3bazFExOV1+WIGwAKQ3EDQGEobgAoDMUNAIWhuAGgMBQ3ABSG4gaAwlDcAFAYihsAClPlnpMrbD9i+4Dt/bY39yMYAKC3Kpe8n5J0a5K9tt8v6UnbDyV5tuFsAIAe5j3iTnIkyd7u4zckHZA03nQwAEBvZzTHbftSSWsl7WkiDABgfpWL2/a5ku6XdEuS0z4ez/Ym25O2J4+dfLvOjACAGSoVt+0RdUp7e5KdvdZJsiXJRJKJi0aG6swIAJihylkllnSXpANJbm8+EgDg3VQ54l4n6QZJV9t+uvv1+YZzAQDmMO/pgEkek+Q+ZAEAVMCVkwBQGIobAApDcQNAYShuACgMxQ0AhaG4AaAwFDcAFIbiBoDCUNwAUJgqN1I4YyeySIdOrmxi0z1dft+DfRvr/8NbAzzesT6OJUn7+zwe6vTndZ/r21gnDj5feV2OuAGgMBQ3ABSG4gaAwlDcAFAYihsACkNxA0BhKG4AKAzFDQCFobgBoDBV7vK+2PYfbD9je7/tH/QjGACgtyqXvJ+QdHWSN22PSHrM9q+S/L7hbACAHqrc5T2S3uw+Hel+pclQAIC5VZrjtj1k+2lJRyU9lGRPs7EAAHOpVNxJ3k7yCUnLJV1p+4rZ69jeZHvS9uQrp6brzgkA6Dqjs0qSvCbpt5LW9/izLUkmkkxcMDxaUzwAwGxVziq5yPb53cdjkq6VdLDpYACA3qqcVXKJpG22h9Qp+vuS/LLZWACAuVQ5q+SPktb2IQsAoAKunASAwlDcAFAYihsACkNxA0BhKG4AKAzFDQCFobgBoDAUNwAUhuIGgMK483HbNW/UPibpb+/hr14o6Z81x1koBnnfJPavdOxf+z6U5KIqKzZS3O+V7ckkE23naMIg75vE/pWO/SsLUyUAUBiKGwAKs9CKe0vbARo0yPsmsX+lY/8KsqDmuAEA81toR9wAgHksiOK2vd72c7YP2b6t7Tx1sr3C9iO2D9jeb3tz25nqZnvI9lO2B+7OSLbPt73D9sHuz/BTbWeqk+1vd38v99m+x/bitjOdDdtbbR+1vW/GsgtsP2T7+e73pW1mrEPrxd29Jdodkq6TtFrSRtur201Vq1OSbk3yUUlXSfrGgO2fJG2WdKDtEA35iaRfJ/mIpDUaoP20PS7pW5ImklwhaUjS9e2mOmt36/Sbmd8m6eEkl0l6uPu8aK0Xt6QrJR1KcjjJtKR7JW1oOVNtkhxJsrf7+A11/uGPt5uqPraXS/qCpDvbzlI32+dJ+oykuyQpyXSS19pNVbthSWO2hyUtkfRyy3nOSpJHJb0ya/EGSdu6j7dJ+nJfQzVgIRT3uKQXZzyf0gAV20y2L1Xn/p172k1Sqx9L+o6k/7QdpAEflnRM0s+6U0F32j6n7VB1SfKSpB9JekHSEUn/SvJgu6ka8YEkR6TOgZSkZS3nOWsLobjdY9nAnepi+1xJ90u6Jcnrbeepg+0vSjqa5Mm2szRkWNInJf00yVpJb2kAXma/ozvXu0HSSkkflHSO7a+1mwpVLITinpK0Ysbz5Sr85dpstkfUKe3tSXa2nadG6yR9yfZf1Zniutr2z9uNVKspSVNJ3nmFtEOdIh8U10r6S5JjSU5K2inp0y1nasI/bF8iSd3vR1vOc9YWQnE/Ieky2yttj6rz5siuljPVxrbVmSM9kOT2tvPUKcl3kyxPcqk6P7ffJBmYI7Ykf5f0ou1V3UXXSHq2xUh1e0HSVbaXdH9Pr9EAvfk6wy5JN3Yf3yjpFy1mqcVw2wGSnLJ9s6QH1HlXe2uS/S3HqtM6STdI+pPtp7vLvpdkd4uZUN03JW3vHlQclvT1lvPUJske2zsk7VXn7KenVPgVhrbvkfRZSRfanpL0fUk/lHSf7ZvU+c/qq+0lrAdXTgJAYRbCVAkA4AxQ3ABQGIobAApDcQNAYShuACgMxQ0AhaG4AaAwFDcAFOa/tr8BPbmNn/UAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7f07ad156390>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Training using SARSA method:\n",
      "\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -16.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -14.0\n",
      "During testing, average score is  -14.0\n",
      "During testing, average score is  -14.0\n",
      "During testing, average score is  -14.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -14.0\n",
      "During testing, average score is  -14.0\n",
      "During testing, average score is  -100.0\n",
      "During testing, average score is  -14.0\n",
      "During testing, average score is  -14.0\n",
      "During testing, average score is  -14.0\n",
      "During testing, average score is  -14.0\n",
      "During testing, average score is  -14.0\n",
      "Using SARSA generated best score:  -14.0\n",
      "Average of scores during training: -31.0895\n",
      "Episode run using the greedy policy and the final action-value function\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAW4AAACSCAYAAABlhSBZAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAACNRJREFUeJzt3X+oXnUBx/HPx3vvuJtOTeaGbcspDW0MyriINYhQoVnRCgoUEglh/2RqCGH9E/3XHyEVSLB0KSSKqJGEpWKKiDW9zlWb01pq8+ZyC3HOaD+uffrjeYTb9bneM3d+3O/T+wWX+zznnp3v53Dv/ew85znnfp1EAIBynNR1AADA8aG4AaAwFDcAFIbiBoDCUNwAUBiKGwAKQ3EDQGEobgAoDMUNAIUZbWKjp42NZ8WipU1sGgCG0mtHD+ngscOusm4jxb1i0VL9eP2Xmtg0AAyla3f+ovK6nCoBgMJQ3ABQGIobAApDcQNAYShuACgMxQ0AhaG4AaAwFDcAFIbiBoDCVCpu2xttv2B7j+0bmw4FAJjbvMVte0TSzZIuk7RO0hW21zUdDAAwWJUj7gsl7UnyYpKjku6StKnZWACAuVQp7pWSXpnxfKq/7H/Y3mx70vbkwenDdeUDAMxSpbgH/ZnBvGtBsiXJRJKJ00bHTzwZAGCgKsU9JWn1jOerJL3aTBwAwHyqFPfTktbaPsf2IkmXS7q/2VgAgLnMO5FCkmnb10h6UNKIpK1JdjWeDAAwUKUZcJI8IOmBhrMAACrgzkkAKAzFDQCFobgBoDAUNwAUhuIGgMJQ3ABQGIobAApDcQNAYSrdgHO8Tj3pkD6z5LEmNj3Q4WVvtTaWJP17+aFWxzu8vN3907KW/0jY8sUtjrWkvbEk6ZQ17Y639MOtDueWxxsfP7/V8RYvXt/aWEs3PFl5XY64AaAwFDcAFIbiBoDCUNwAUBiKGwAKQ3EDQGEobgAoDMUNAIWhuAGgMBQ3ABRm3uK2vdX2fts72wgEAHhvVY64b5O0seEcAICK5i3uJI9Ler2FLACACmo7x217s+1J25MHjr1d12YBALPUVtxJtiSZSDJx5thIXZsFAMzCVSUAUBiKGwAKU+VywDsl/U7SebanbF/dfCwAwFzmnbosyRVtBAEAVMOpEgAoDMUNAIWhuAGgMBQ3ABSG4gaAwlDcAFAYihsACkNxA0Bh5r0B5/04kkV66diHmtj0QH/ee25rY0mS9rY7HLBwtf3L0O54G7d9ubWx7PHK63LEDQCFobgBoDAUNwAUhuIGgMJQ3ABQGIobAApDcQNAYShuACgMxQ0Ahaky5+Rq24/a3m17l+3r2ggGABisyi3v05JuSLLd9lJJz9h+OMlzDWcDAAww7xF3kn1JtvcfH5K0W9LKpoMBAAY7rnPcttdIukDStibCAADmV7m4bZ8i6V5J1yd5c8DXN9uetD35+vSxOjMCAGaoVNy2x9Qr7TuS3DdonSRbkkwkmThjdKzOjACAGapcVWJJt0raneSm5iMBAN5LlSPuDZKulHSx7R39j882nAsAMId5LwdM8oQkt5AFAFABd04CQGEobgAoDMUNAIWhuAGgMBQ3ABSG4gaAwlDcAFAYihsACkNxA0BhqkykcNyOaJH+On12E5se6LKnftraWAD+fzx00dutjfXm8y9XXpcjbgAoDMUNAIWhuAGgMBQ3ABSG4gaAwlDcAFAYihsACkNxA0BhKG4AKEyVWd7HbT9l+w+2d9n+XhvBAACDVbnl/Yiki5O8ZXtM0hO2f53k9w1nAwAMUGWW90h6q/90rP+RJkMBAOZW6Ry37RHbOyTtl/Rwkm3NxgIAzKVScSd5O8nHJK2SdKHt9bPXsb3Z9qTtyYPHDtedEwDQd1xXlSR5Q9JjkjYO+NqWJBNJJk4bG68pHgBgtipXlZxp+/T+48WSLpX0fNPBAACDVbmq5CxJt9seUa/o707yq2ZjAQDmUuWqkj9KuqCFLACACrhzEgAKQ3EDQGEobgAoDMUNAIWhuAGgMBQ3ABSG4gaAwlDcAFAYihsACuPen9uueaP2AUl/ex//dJmkf9YcZ6EY5n2T2L/SsX/dOzvJmVVWbKS43y/bk0kmus7RhGHeN4n9Kx37VxZOlQBAYShuACjMQivuLV0HaNAw75vE/pWO/SvIgjrHDQCY30I74gYAzGNBFLftjbZfsL3H9o1d56mT7dW2H7W92/Yu29d1nalutkdsP2t76GZGsn267XtsP9//Hn6i60x1sv3N/s/lTtt32i56wljbW23vt71zxrIzbD9s+y/9zx/oMmMdOi/u/pRoN0u6TNI6SVfYXtdtqlpNS7ohyUckXSTp60O2f5J0naTdXYdoyI8k/SbJ+ZI+qiHaT9srJV0raSLJekkjki7vNtUJu03vnsz8RkmPJFkr6ZH+86J1XtySLpS0J8mLSY5KukvSpo4z1SbJviTb+48PqfeLv7LbVPWxvUrS5yTd0nWWutk+VdKnJN0qSUmOJnmj21S1G5W02PaopCWSXu04zwlJ8rik12ct3iTp9v7j2yV9sdVQDVgIxb1S0isznk9piIptJttr1Ju/c1u3SWr1Q0nfkvSfroM04FxJByT9rH8q6BbbJ3cdqi5J/i7pB5L2Ston6WCSh7pN1YgVSfZJvQMpScs7znPCFkJxe8CyobvUxfYpku6VdH2SN7vOUwfbn5e0P8kzXWdpyKikj0v6SZILJP1LQ/Ay+x39c72bJJ0j6YOSTrb91W5ToYqFUNxTklbPeL5Khb9cm832mHqlfUeS+7rOU6MNkr5g+2X1TnFdbPvn3Uaq1ZSkqSTvvEK6R70iHxaXSnopyYEkxyTdJ+mTHWdqwmu2z5Kk/uf9Hec5YQuhuJ+WtNb2ObYXqffmyP0dZ6qNbat3jnR3kpu6zlOnJN9OsirJGvW+b79NMjRHbEn+IekV2+f1F10i6bkOI9Vtr6SLbC/p/5xeoiF683WG+yVd1X98laRfdpilFqNdB0gybfsaSQ+q96721iS7Oo5Vpw2SrpT0J9s7+su+k+SBDjOhum9IuqN/UPGipK91nKc2SbbZvkfSdvWufnpWhd9haPtOSZ+WtMz2lKTvSvq+pLttX63ef1Zf6S5hPbhzEgAKsxBOlQAAjgPFDQCFobgBoDAUNwAUhuIGgMJQ3ABQGIobAApDcQNAYf4LG7gHoisGTZ8AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7f07aae4e6d8>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    " for learning_method in [\"QL\", \"SARSA\"]:\n",
    "\n",
    "    agent = Agent([4,12], learning_method) #create an agent that also creates the enviroment\n",
    "\n",
    "    best_score = -np.inf\n",
    "    best_episode = []\n",
    "    total_score = 0\n",
    "\n",
    "    agent.print_method() #print method name\n",
    "\n",
    "    for i in range(total_episodes):\n",
    "        episode, score, done = agent.generate_episode()  #generate an episode and return the results\n",
    "        total_score += score\n",
    "\n",
    "        if score > best_score: #log the best score and episode history\n",
    "            best_score = score\n",
    "            best_episode = episode\n",
    "\n",
    "        agent.test(i) #every eval_frequency episodes test our policy\n",
    "        #agent.reduce_epsilon()\n",
    "\n",
    "    agent.print_best(best_score) #print best score\n",
    "    #agent.print_game(best_episode) #show history of best episode\n",
    "    print(\"Average of scores during training:\", total_score/total_episodes) #print average training score\n",
    "    \n",
    "    print(\"Episode run using the greedy policy and the final action-value function\")\n",
    "    history, score, done = agent.generate_test_episode()\n",
    "    agent.print_game(history) #show history of test episode\n",
    "        "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
