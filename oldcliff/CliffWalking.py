# coding: utf-8

import numpy as np
import random as rand
import copy
import matplotlib.pyplot as plt

class CliffWalking(): #The enviroment
    def __init__(self, size):
        self.goal_reward = 0
        self.cliff_reward = -100
        self.grid_size = size
        self.starting_position = [self.grid_size[0]-1, 0]
        self.grid = self.create_grid()
        self.done = False
        
    def create_grid(self):
        grid = np.ones(self.grid_size) * (-1)
        grid[-1, -1] = self.goal_reward
        grid[-1, 1:-1] = self.cliff_reward
        return grid

    def get_reward(self, action): #Set the new state and return reward        
        new_state = copy.copy(self.state)
        
        if action == 0: #Move down
            new_state[0] -= 1
        elif action == 1: # Move right
            new_state[1] += 1
        elif action == 2: # Move up
            new_state[0] += 1
        elif action == 3: # Move left
            new_state[1] -= 1
        else:
            raise Exception
        
        inside_grid = (0 <= new_state[1] < self.grid_size[1]) and (0 <= new_state[0] < self.grid_size[0])
        
        if inside_grid:
            reward = self.grid[new_state[0],new_state[1]]
            self.state = new_state
        else:
            reward = -1 #reward for bouncing
            
        return reward

    def reset(self): #Reinitialize the enviroment to start over
        self.state = copy.copy(self.starting_position)
        self.previous_reward = -1
        self.done = False
        return self.state[0], self.state[1]

    def step(self, action): #Perform an action on the enviroment and observe the results
        if self.done:
            raise Exception
        reward = self.get_reward(action)
        if (reward == self.cliff_reward) or (reward == self.goal_reward):
            self.done = True
        else:
            self.done = False
        return (self.state[0], self.state[1]), reward, self.done

    def visualize(self,history): #Visualize an episode
        track = copy.copy(self.create_grid())
        i = 1.0
        for state, _, _ in history:
            track[state[0],state[1]] += i
            i+=10
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.set_aspect('equal')
        plt.imshow(track, interpolation='nearest', cmap=plt.cm.CMRmap)
        plt.show()



